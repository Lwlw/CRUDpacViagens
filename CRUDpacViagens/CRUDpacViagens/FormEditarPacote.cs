﻿namespace CRUDpacViagens
{
    using CRUDpacViagens.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormEditarPacote : Form
    {
        private List<Pacote> listaEditaPacote = new List<Pacote>();

        public FormEditarPacote(List<Pacote> listaDePacotes, int index)
        {
            InitializeComponent();
            listaEditaPacote = listaDePacotes;

            textBoxID.Text = listaEditaPacote[index].IdPacote.ToString();
            textBoxDescricao.Text = listaEditaPacote[index].Descricao;
            textBoxPreco.Text = listaEditaPacote[index].Preco.ToString();
        }


        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxID.Text))
            {
                MessageBox.Show("Insira id do pacote");
                return;
            }

            if (string.IsNullOrEmpty(textBoxDescricao.Text))
            {
                MessageBox.Show("Insira a descrição do pacote");
                return;
            }

            if (string.IsNullOrEmpty(textBoxPreco.Text))
            {
                MessageBox.Show("Insira o preço do pacote");
                return;
            }


            int index = Convert.ToInt32(textBoxID.Text);
            if (listaEditaPacote[index - 1] != null)
            {
                listaEditaPacote[index - 1].Descricao = textBoxDescricao.Text;
                listaEditaPacote[index - 1].Preco = Convert.ToDecimal(textBoxPreco.Text);
                MessageBox.Show("Pacote editado com sucesso!");

                Close();

            }
        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O Pacote não foi editado.");
            Close();
        }



        private void buttonNext_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(textBoxID.Text) - 1;
            index++;
            if (index > listaEditaPacote.Count - 1)
                index = 0;

            textBoxID.Text = listaEditaPacote[index].IdPacote.ToString();
            textBoxDescricao.Text = listaEditaPacote[index].Descricao;
            textBoxPreco.Text = listaEditaPacote[index].Preco.ToString();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(textBoxID.Text) - 1;
            index--;
            if (index < 0)
                index = listaEditaPacote.Count - 1;

            textBoxID.Text = listaEditaPacote[index].IdPacote.ToString();
            textBoxDescricao.Text = listaEditaPacote[index].Descricao;
            textBoxPreco.Text = listaEditaPacote[index].Preco.ToString();
        }
    }
}
