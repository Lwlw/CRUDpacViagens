﻿namespace CRUDpacViagens
{
    using CRUDPacViagens;
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormApagarPacote : Form
    {
        private List<Pacote> listaDePacotes = new List<Pacote>();
        public FormApagarPacote(List<Pacote> listaPacotes, int pack)
        {
            InitializeComponent();

            foreach (var pacote in listaPacotes)
            {
                ComboListaPacs.Items.Add(pacote);
            }
            ComboListaPacs.SelectedIndex = pack;

            this.listaDePacotes = listaPacotes;
        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O pacote não foi apagado.");
            Close();
        }

        private void buttonCriar_Click(object sender, EventArgs e)
        {
            int index = 0;

            foreach (var pacote in listaDePacotes)
            {
                if (ComboListaPacs.SelectedItem == pacote)
                {
                    listaDePacotes.RemoveAt(index);
                    
                    break;
                }

                index++;
            }

            MessageBox.Show("Pacote apagado com sucesso.");

            Close();
        }
    }
}
