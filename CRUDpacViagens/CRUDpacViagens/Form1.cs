﻿namespace CRUDPacViagens
{
    using System.IO;
    using CRUDpacViagens;
    using CRUDpacViagens.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private List<Pacote> listaDePacotes;

        private FormNovoPacote FormularioNovoPacote;

        private FormEditarPacote FormularioEditarPacote;

        private FormLerPacote FormularioLerPacote;

        private FormApagarPacote FormularioApagarPacote;

        string file_ = @"Pacotes.txt";

        private void togglButtons()
        {
            if (listaDePacotes.Count == 0)
            {
                buttonEditar.Enabled = false;
                buttonLer.Enabled = false;
                buttonApagar.Enabled = false;
                ComboListaPacs.Enabled = false;
                labelSele.Enabled = false;
                return;
            }
                buttonEditar.Enabled = true;
                buttonLer.Enabled = true;
                buttonApagar.Enabled = true;
                ComboListaPacs.Enabled = false;
                labelSele.Enabled = false;
        }

        public void CustomWindow()
        {
            this.FormBorderStyle = FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            // Remove the control box so the form will only display client area.
            this.ControlBox = false;
            
        }

        public Form1()
        {
            InitializeComponent();

            listaDePacotes = new List<Pacote>();

            if (!CarregarPacotes())
            {
            listaDePacotes.Add(new Pacote { IdPacote = 0001, Descricao = "Num Fax para Tokyo", Preco = 500 });
            listaDePacotes.Add(new Pacote { IdPacote = 0002, Descricao = "Chamada a Bruxelas", Preco = 450 });
            listaDePacotes.Add(new Pacote { IdPacote = 0003, Descricao = "O Porto aqui tão perto", Preco = 300 });
            listaDePacotes.Add(new Pacote { IdPacote = 0004, Descricao = "Give me more Lismore", Preco = 450 });
            listaDePacotes.Add(new Pacote { IdPacote = 0005, Descricao = "Berlim: Quebre aos murros as barreiras", Preco = 600 });
            listaDePacotes.Add(new Pacote { IdPacote = 0006, Descricao = "Dengue é Rio de Janeiro", Preco = 650 });
            listaDePacotes.Add(new Pacote { IdPacote = 0007, Descricao = "Algarve cámone yeah! hee-haw", Preco = 500 });
            listaDePacotes.Add(new Pacote { IdPacote = 0008, Descricao = "Cochinchina é que é", Preco = 900 });
            }

            //GravarAlunos();

            //ComboListaPacs.DataSource = ListaDePacotes;

            foreach (var pacote in listaDePacotes)
            {
                ComboListaPacs.Items.Add($"Pacote {pacote.IdPacote}");
            }
            ComboListaPacs.SelectedIndex = 0;

            buttonCriar.Text = $"Criar Pacote\n{idNewPac()}";

            labelInfoFile.Text = file_.Contains("\\") ? 
                $"File: {file_.Split('\\')[file_.Split('\\').Length - 1]}" : $@"File: {file_}";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon.");
        }


        
        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            if (listaDePacotes.Count == 0)
            {
                MessageBox.Show("Sem pacotes para apagar");
                return;
            }
            FormularioApagarPacote = new FormApagarPacote(listaDePacotes, ComboListaPacs.SelectedIndex);
            FormularioApagarPacote.Show();
        }
        private void ButtonLer_Click(object sender, EventArgs e)
        {
            if (listaDePacotes.Count == 0)
            {
                MessageBox.Show("Sem pacotes para mostrar");
                return;
            }
            FormularioLerPacote = new FormLerPacote(listaDePacotes);
            FormularioLerPacote.Show();
        }
        private void ButtonEditar_Click(object sender, EventArgs e)
        {
            if (listaDePacotes.Count == 0)
            {
                MessageBox.Show("Sem pacotes para editar");
                return;
            }

            FormularioEditarPacote = new FormEditarPacote(listaDePacotes, ComboListaPacs.SelectedIndex);
            FormularioEditarPacote.Show();
        }

        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            FormularioNovoPacote = new FormNovoPacote(listaDePacotes);
            FormularioNovoPacote.Show();
        }



        private void actualizaCombos()
        {
            //ComboListaPacs.DataSource = null;
            //ComboListaPacs.DataSource = listaDePacotes;

            ComboListaPacs.Items.Clear();

            if (listaDePacotes.Count == 0)
            {
                buttonCriar.Text = "Criar Pacote\n1";
                labelInfoFile.Text = $"File: {file_.Split('\\')[file_.Split('\\').Length - 1]}";
                return;
            }

            foreach (var pacote in listaDePacotes)
            {
                ComboListaPacs.Items.Add($"Pacote {pacote.IdPacote}");
            }
            ComboListaPacs.SelectedIndex = 0;

            buttonCriar.Text = $"Criar Pacote\n{idNewPac()}";

            labelInfoFile.Text = file_.Contains("\\") ?
                $"File: {file_.Split('\\')[file_.Split('\\').Length - 1]}" : $@"File: {file_}";
        }



        private bool GravarPacotes()
        {
            string ficheiro = file_;

            StreamWriter sw = new StreamWriter(ficheiro, false);

            try
            {
                if (!File.Exists(ficheiro))
                    sw = File.CreateText(ficheiro);

                foreach (var pack in listaDePacotes)
                {
                    string linha = string.Format("{0};{1};{2}", pack.IdPacote, pack.Descricao, pack.Preco);
                    sw.WriteLine(linha);

                }

                sw.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

                return false;
            }

            return true;
        }

        private bool CarregarPacotes()
        {
            string ficheiro = file_;

            StreamReader sr;

            try
            {
                if (File.Exists(ficheiro))
                {
                    sr = File.OpenText(ficheiro);

                    string linha = "";

                    while ((linha = sr.ReadLine()) != null)
                    {
                        string[] campos = new string[3];

                        campos = linha.Split(';');

                        var pacote = new Pacote
                        {
                            IdPacote = Convert.ToInt32(campos[0]),
                            Descricao = campos[1],
                            Preco = Convert.ToDecimal(campos[2])
                        };

                        listaDePacotes.Add(pacote);
                    }

                    sr.Close();
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }

            return true;
        }

        //private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    actualizaCombos();
        //    MessageBox.Show("Alunos atualizados com sucesso.");
        //}


        private int idNewPac()
        {
            return listaDePacotes[listaDePacotes.Count - 1].IdPacote + 1;
        }


        private void Form1_Activated(object sender, EventArgs e)  //switches betw windows
        {
            actualizaCombos();
            GravarPacotes();
            togglButtons();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (GravarPacotes())
            {
                MessageBox.Show("Alunos gravados com successo");
            }

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Stream myStream;
            //SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            //saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.DefaultExt = "txt";
            saveFileDialog1.Title = "Save As";
            string oldFile = file_;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK) //or eventHandler
            {

                file_ = saveFileDialog1.FileName;
                //File.WriteAllText(file_, "");
                //listaDePacotes.Clear();
                try
                {
                    System.IO.File.Copy(oldFile, file_);
                }
                catch (Exception)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you really really want to overwrite the file?", "Overwrite?", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        File.Delete(file_);
                        File.Copy(oldFile, file_);
                    }


                }

                actualizaCombos();
                togglButtons();
                //if ((myStream = saveFileDialog1.OpenFile()) != null)
                //{
                //    // Code to write the stream goes here. No. There.
                //------------------------------------
                //    using (StreamWriter sw = new StreamWriter(savefile.FileName))
                //    sw.WriteLine("Hello World!");
                //------------------------------------
                //    myStream.Close();
                //}

            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            //saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.DefaultExt = "txt";
            

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //System.IO.StreamReader sr = new
                //   System.IO.StreamReader(openFileDialog1.FileName);          
                //MessageBox.Show(sr.ReadToEnd());
                //sr.Close();
                file_ = openFileDialog1.FileName;
                ComboListaPacs.Items.Clear();
                listaDePacotes.Clear();
                CarregarPacotes();
                actualizaCombos();

                togglButtons();
            }
        }

        private void saveNewToolStripMenuItem_Click(object sender, EventArgs e)
        {

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.DefaultExt = "txt";
            string oldFile = file_;
            saveFileDialog1.Title = "Save New";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK) //or eventHandler
            {

                file_ = saveFileDialog1.FileName;
                File.WriteAllText(file_, "");
                ComboListaPacs.Items.Clear();
                listaDePacotes.Clear();
                CarregarPacotes();
                actualizaCombos();

                togglButtons();

            }
        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}

