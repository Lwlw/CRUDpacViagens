﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDpacViagens.Modelos
{
    public class Pacote
    {
        public int IdPacote { get; set; }

        public string Descricao { get; set; }

        public decimal Preco { get; set; }

        //public string NomeCompleto
        //{
        //    get { return string.Format("{0} {1}", PrimeiroNome, Apelido); }
        //}

        #region Métodos Genéricos

        public override int GetHashCode()
        {
            return IdPacote;
        }
        public override string ToString()
        {
            return string.Format("Pacote {0} - {1} - Preço: {2}€",
                IdPacote, Descricao, Preco);
        }

        #endregion

    }
}
