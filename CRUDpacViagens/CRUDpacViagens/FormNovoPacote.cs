﻿namespace CRUDpacViagens
{
    using CRUDpacViagens.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormNovoPacote : Form
    {
        private List<Pacote> listaDePacotes = new List<Pacote>();

        public FormNovoPacote(List<Pacote> listaDePacotes)
        {
            InitializeComponent();
            this.listaDePacotes = listaDePacotes;
            labelID.Text = "ID: " + GeraIdPacote();
        }

        private void buttonCriar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxDescricao.Text))
            {
                MessageBox.Show("Tem que inserir a descricao do pacote.");
                return;
            }

            if (string.IsNullOrEmpty(textBoxPreco.Text))
            {
                MessageBox.Show("Tem que inserir o preco do pacote.");
                return;
            }

            var pacote = new Pacote
            {
                IdPacote = GeraIdPacote(),
                Descricao = textBoxDescricao.Text,
                Preco = Convert.ToDecimal(textBoxPreco.Text)
            };

            listaDePacotes.Add(pacote);
            MessageBox.Show("Novo pacote criado com sucesso");
            Close();
        }

        public FormNovoPacote()
        {
            InitializeComponent();
        }

        private int GeraIdPacote()
        {
            if (listaDePacotes.Count == 0)
                return 1;
            return listaDePacotes[listaDePacotes.Count - 1].IdPacote + 1;
        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
