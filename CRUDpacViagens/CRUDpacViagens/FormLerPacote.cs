﻿namespace CRUDpacViagens
{
    using CRUDpacViagens.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormLerPacote : Form
    {
        private List<Pacote> listaDePacotes = new List<Pacote>();
        public FormLerPacote(List<Pacote> listaDePacotes)
        {
            InitializeComponent();


            //dataGridViewPacs.DataSource = listaDePacotes;

            //DataTable dt = new DataTable();
            //dt.Columns.AddRange(new DataColumn[3] { new DataColumn("Id", typeof(int)),
            //                    new DataColumn("Name", typeof(string)),
            //                    new DataColumn("Country",typeof(string)) });
            //dt.Rows.Add(1, "John Hammond", "United States");
            //dt.Rows.Add(2, "Mudassar Khan", "India");
            //dt.Rows.Add(3, "Suzanne Mathews", "France");
            //dt.Rows.Add(4, "Robert Schidner", "Russia");
            //this.dataGridView1.DataSource = dt;

            //for more control
            dataGridViewPacs.ColumnCount = 3;
            dataGridViewPacs.Columns[0].Name = "ID";
            dataGridViewPacs.Columns[1].Name = "Descrição";
            dataGridViewPacs.Columns[2].Name = "Preço";
            dataGridViewPacs.RowHeadersVisible = false;
            this.dataGridViewPacs.AllowUserToAddRows = false;

            string[] row;
            foreach (var pacote in listaDePacotes)
            {
                row = new string[] { pacote.IdPacote.ToString(), pacote.Descricao, pacote.Preco.ToString() + "€" };
                dataGridViewPacs.Rows.Add(row);
            }

            dataGridViewPacs.Columns[0].Width = 35;
            dataGridViewPacs.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridViewPacs.Columns[1].Width = 340;
            dataGridViewPacs.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            if (dataGridViewPacs.RowCount > 7)
                dataGridViewPacs.Columns[2].Width = 65;
            else
                dataGridViewPacs.Columns[2].Width = 82;
            dataGridViewPacs.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
