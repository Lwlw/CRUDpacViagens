﻿namespace CRUDpacViagens
{
    partial class FormLerPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewPacs = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPacs)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewPacs
            // 
            this.dataGridViewPacs.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewPacs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPacs.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewPacs.Location = new System.Drawing.Point(52, 85);
            this.dataGridViewPacs.Name = "dataGridViewPacs";
            this.dataGridViewPacs.RowTemplate.Height = 28;
            this.dataGridViewPacs.Size = new System.Drawing.Size(690, 325);
            this.dataGridViewPacs.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(284, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Pacotes de Viagem";
            // 
            // buttonSair
            // 
            this.buttonSair.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonSair.BackgroundImage = global::CRUDpacViagens.Properties.Resources.ic_cancel;
            this.buttonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonSair.Location = new System.Drawing.Point(670, 432);
            this.buttonSair.Name = "buttonSair";
            this.buttonSair.Size = new System.Drawing.Size(75, 60);
            this.buttonSair.TabIndex = 10;
            this.buttonSair.Tag = "Sair";
            this.buttonSair.UseVisualStyleBackColor = false;
            this.buttonSair.Click += new System.EventHandler(this.buttonSair_Click);
            // 
            // FormLerPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScrollMargin = new System.Drawing.Size(15, 0);
            this.ClientSize = new System.Drawing.Size(802, 515);
            this.Controls.Add(this.buttonSair);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewPacs);
            this.Name = "FormLerPacote";
            this.Text = "FormLerPacote";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPacs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewPacs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSair;
    }
}