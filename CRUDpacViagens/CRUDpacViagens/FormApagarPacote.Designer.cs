﻿namespace CRUDpacViagens
{
    partial class FormApagarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ComboListaPacs = new System.Windows.Forms.ComboBox();
            this.buttonSair = new System.Windows.Forms.Button();
            this.buttonCriar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(190, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Apagar Pacotes";
            // 
            // ComboListaPacs
            // 
            this.ComboListaPacs.AllowDrop = true;
            this.ComboListaPacs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboListaPacs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboListaPacs.FormattingEnabled = true;
            this.ComboListaPacs.Location = new System.Drawing.Point(46, 118);
            this.ComboListaPacs.Name = "ComboListaPacs";
            this.ComboListaPacs.Size = new System.Drawing.Size(469, 28);
            this.ComboListaPacs.TabIndex = 8;
            // 
            // buttonSair
            // 
            this.buttonSair.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonSair.BackgroundImage = global::CRUDpacViagens.Properties.Resources.ic_cancel;
            this.buttonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonSair.Location = new System.Drawing.Point(440, 266);
            this.buttonSair.Name = "buttonSair";
            this.buttonSair.Size = new System.Drawing.Size(75, 60);
            this.buttonSair.TabIndex = 20;
            this.buttonSair.UseVisualStyleBackColor = false;
            this.buttonSair.Click += new System.EventHandler(this.buttonSair_Click);
            // 
            // buttonCriar
            // 
            this.buttonCriar.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonCriar.BackgroundImage = global::CRUDpacViagens.Properties.Resources.ic_ok;
            this.buttonCriar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonCriar.Location = new System.Drawing.Point(336, 266);
            this.buttonCriar.Name = "buttonCriar";
            this.buttonCriar.Size = new System.Drawing.Size(75, 60);
            this.buttonCriar.TabIndex = 19;
            this.buttonCriar.UseVisualStyleBackColor = false;
            this.buttonCriar.Click += new System.EventHandler(this.buttonCriar_Click);
            // 
            // FormApagarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 377);
            this.Controls.Add(this.buttonSair);
            this.Controls.Add(this.buttonCriar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboListaPacs);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormApagarPacote";
            this.Text = "FormApagarPacote";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboListaPacs;
        private System.Windows.Forms.Button buttonSair;
        private System.Windows.Forms.Button buttonCriar;
    }
}