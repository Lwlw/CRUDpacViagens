﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace teste
{
    public partial class Form1 : Form
    {
        List<int> listazita = new List<int>() { 1, 2, 3, 4, 5 };
        public Form1()
        {
            InitializeComponent();
            

            textBox1.Text = listazita[0].ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(textBox1.Text) - 1;
            index++;
            if (index > listazita.Count - 1)
                index = 0;

            textBox1.Text = listazita[index].ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(textBox1.Text) - 1;
            index--;
            if (index < 0)
                index = listazita.Count - 1;

            textBox1.Text = listazita[index].ToString();
        }
    }
}
