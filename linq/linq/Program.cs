﻿using LivrariaDeClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Aluno> Alunos = ListaDeAlunos.LoadAlunos();


            //Ordenar por apelido
            //Alunos = Alunos.OrderByDescending(x => x.Apelido).ThenByDescending(x => x.DisciplinasFeitas).ToList();
            //Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month == 3).ToList();

            //foreach (var aluno in Alunos)
            //    Console.WriteLine($"{aluno.PrimeiroNome} {aluno.Apelido} {aluno.DataNascimento.ToShortDateString()} {aluno.DisciplinasFeitas}");


            int totalDisciplinasFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            double mediaDisciplinasFeitas = Alunos.Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas: {totalDisciplinasFeitas}");
            Console.WriteLine($"Média de disciplinas feitas: {mediaDisciplinasFeitas:N2}");
            Console.ReadKey();
        }
    }
}
