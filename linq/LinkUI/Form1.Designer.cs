﻿namespace LinkUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxTodosAlunos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownFeitas = new System.Windows.Forms.NumericUpDown();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxFiltro = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFeitas)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Todos os alunos";
            // 
            // comboBoxTodosAlunos
            // 
            this.comboBoxTodosAlunos.FormattingEnabled = true;
            this.comboBoxTodosAlunos.Location = new System.Drawing.Point(15, 35);
            this.comboBoxTodosAlunos.Name = "comboBoxTodosAlunos";
            this.comboBoxTodosAlunos.Size = new System.Drawing.Size(134, 21);
            this.comboBoxTodosAlunos.TabIndex = 1;
            this.comboBoxTodosAlunos.SelectedIndexChanged += new System.EventHandler(this.comboBoxTodosAlunos_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Disciplinas feitas";
            // 
            // numericUpDownFeitas
            // 
            this.numericUpDownFeitas.Location = new System.Drawing.Point(103, 202);
            this.numericUpDownFeitas.Name = "numericUpDownFeitas";
            this.numericUpDownFeitas.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownFeitas.TabIndex = 3;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(15, 242);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(82, 30);
            this.buttonUpdate.TabIndex = 4;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(172, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Alunos filtrados";
            // 
            // listBoxFiltro
            // 
            this.listBoxFiltro.FormattingEnabled = true;
            this.listBoxFiltro.Location = new System.Drawing.Point(175, 35);
            this.listBoxFiltro.Name = "listBoxFiltro";
            this.listBoxFiltro.Size = new System.Drawing.Size(170, 251);
            this.listBoxFiltro.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 304);
            this.Controls.Add(this.listBoxFiltro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.numericUpDownFeitas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxTodosAlunos);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "FormLink";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFeitas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTodosAlunos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownFeitas;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxFiltro;
    }
}

