﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivrariaDeClasses
{
    public class ListaDeAlunos
    {
        public static List<Aluno> LoadAlunos()
        {
            List<Aluno> output = new List<Aluno>();

            output.Add(new Aluno { PrimeiroNome = "Torres", Apelido = "Carlos", DataNascimento = Convert.ToDateTime("25/02/1970"), DisciplinasFeitas = 20 });
            output.Add(new Aluno { PrimeiroNome = "Jesuita", Apelido = "Susana", DataNascimento = Convert.ToDateTime("31/03/1970"), DisciplinasFeitas = 12 });
            output.Add(new Aluno { PrimeiroNome = "Susana", Apelido = "Sousa", DataNascimento = Convert.ToDateTime("03/01/1970"), DisciplinasFeitas = 1 });
            output.Add(new Aluno { PrimeiroNome = "Sara", Apelido = "Jesuita", DataNascimento = Convert.ToDateTime("06/03/1970"), DisciplinasFeitas = 8 });
            output.Add(new Aluno { PrimeiroNome = "Janota", Apelido = "Duarte", DataNascimento = Convert.ToDateTime("18/02/1970"), DisciplinasFeitas = 7 });
            output.Add(new Aluno { PrimeiroNome = "Maria", Apelido = "Susana", DataNascimento = Convert.ToDateTime("23/01/1970"), DisciplinasFeitas = 16 });

            return output;
        }
    }
}
